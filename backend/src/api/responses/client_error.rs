use anyhow::Error;
use serde::Serialize;

#[derive(Serialize)]
pub struct ClientError {
    pub description: String,
    pub message: String,
}

impl ClientError {
    pub fn new(error: Error, description: &str) -> Self {
        ClientError {
            description: String::from(description),
            message: error.to_string(),
        }
    }
}
