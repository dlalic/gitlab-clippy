#[derive(Debug)]
struct Foo {
    bar: i32,
}

fn main() {
    let bar = 23;
    let foo = Foo { bar: bar };
    println!("{:?}", foo.bar);
}
