# GitLab Clippy

[![pipeline status](https://gitlab.com/dlalic/gitlab-clippy/badges/master/pipeline.svg)](https://gitlab.com/dlalic/gitlab-clippy/-/commits/master)
[![coverage report](https://gitlab.com/dlalic/gitlab-clippy/badges/master/coverage.svg)](https://gitlab.com/dlalic/gitlab-clippy/-/commits/master)
[![Crates.io](https://img.shields.io/crates/v/gitlab-clippy.svg)](https://crates.io/crates/gitlab-clippy)

Convert clippy warnings into GitLab Code Quality report

## Usage

### GitLab CI example

```
clippy:
  stage: lint
  before_script:
    - rustup component add clippy
    - cargo install gitlab_clippy
  script:
    - cargo clippy
  after_script:
    - cargo clippy --message-format=json | gitlab-clippy > gl-code-quality-report.json
  artifacts:
    reports:
      codequality: gl-code-quality-report.json
    expire_in: 1 week
  rules:
    - if: '$CODE_QUALITY_DISABLED'
      when: never
    - if: '$CI_PIPELINE_SOURCE == "push"'
```

Demo of results showing inside a MR can be viewed [here](https://gitlab.com/dlalic/gitlab-clippy/-/merge_requests/1).

### Locally

1. Install with

```
cargo install gitlab_clippy
```

2. Run with

```
cargo clippy --message-format=json | gitlab-clippy
```
